# learn-rust-tourtials


## rust 知识


### 准备工作

#### 安装
  - 命令行安装  
  - vim
  - ubuntu
  - rust --version
  - cargo new hello-world
  - cargo new libhello --lib
  - cargo run, 编译执行
  - cargo build, 编译 
  - cargo check, check
  - cargo clippy, 

### rust基础

#### 通用编程概念
  - 变量
    - 可变性
    - 常量
    - 隐藏
  - 数据类型
    - 基础数据类型
      - bool
      - char
      - 数字类型
      - 数组
      - 自适应类型
    - 复合数据类型
      - 元组
      - 结构体
      - 枚举
    - 字符类型
  - 函数
  - 注释
  - 控制流
#### 所有权

#### 

### rust进阶


### 实战演练



