// 1 rust使用所有权来管理内存， 编译器在编译时就会根据所有权的规则对内存的使用进行检查
// 2 堆栈 
// 编译时数据的类型大小是固定的，就是分配在栈上的
// 编译时数据的类型大小不是固定的，就是分配在堆上的
// 3 作用域
// 4 string 内存回收
// 5 移动
// 6 clone
// 7 栈上数据copy
// 8 函数和作用域
fn main() {
    println!("************************");
    
    let x: i32 = 1;

    {
      let y = 34;
      println!("x = {}", x);
      println!("y = {}", y);
    }

    println!("************************");
    
    {
      let  s1 = String::from("hello");
      // s1.push_str(" world");
      println!("s1 = {}", s1);
      // String 类型离开作用域的时候回调用drop方法

      let s2 = s1;
      //println!("s1 = {}", s1); // s1 move to s2
      println!("s2 = {}", s2);


      let s3 = s2.clone();

      println!("s2 = {}", s2);

      println!("s3 = {}", s3);
    }
    
    println!("************************");
  

    // copy
    let a = 1;
    let b = a;

    println!("a = {}, b = {}", a, b);
    // 常用的具有copy trait 的类型
    // 所有的整型 
    // 浮点数
    // 布尔类型
    // 字符类型
    // 元组
    println!("************************");
}
