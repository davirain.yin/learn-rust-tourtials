const MAX_COUNT: u32 = 34;

fn main() {
    println!("Hello, world!");

    let a = 1;
    let b: u32 = 23;

    println!("type i32, a = {}", a);
    println!("type u32, b = {}", b);

    println!("MAX_COUNT = {}", MAX_COUNT);

    let mut b = 45;

    println!("before mutable, b = {}", b);

    b += 1;
    println!("after mutable, b = {}", b);
}
