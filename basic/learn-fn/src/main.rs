fn main() {
    println!("**************************");

    other_fun();
    let (a, b) = (23i32, 34u32);

    println!("**************************");

    other_fun1(a, b);

    println!("**************************");

    let (c, d) = (2, 3);
    let ret = sum(c, d);
    println!("ret = {}", ret);

    println!("**************************");
    
    let y = {
      let x = 1;
      x +1
    };  
    
    println!("y = {}", y);
    
    println!("**************************");
}


fn other_fun() {
  println!("this is a function");
}
fn other_fun1(a: i32, b: u32) {
  println!("a = {}, b = {}", a, b);
}
fn sum(a: i32, b: i32) -> i32 {
  a + b
}
