// 可恢复错误和不可恢复错误
//
// panic
//
// enum Result<T, E> {
//    Ok(T),
//    Err(E),
// }
//
//
use std::fs::File;

fn main() {
  //let f = File::open("hello.txt");
  //let r = match f {
  //  Ok(file) => file,
  //  Err(error) => panic!("error : {:?}", error),
  // };
  //let f = File::open("hello.txt").unwrap();
  let f = File::open("hello.txt").expect("Failed to open hello.txt");
}
