// 什么时候使用painc 什么时候使用Result
//
// 测试原型 
// 真实的使用的时候使用result
//

use std::io;
use std::io::Read;
use std::fs::File;

fn main() {
    println!("Hello, world!");

    let r = read_username_from_file3();
    match r {
      Ok(s) => println!("s = {}", s),
      Err(e) => println!("error = {}", e),
    }
}


fn read_username_from_file() -> Result<String, io::Error> {
  let f = File::open("hello.txt");
  let mut f  = match f {
    Ok(file) => file,
    Err(error) => return Err(error),
  };

  let mut s = String::new();

  match f.read_to_string(&mut s) {
    Ok(_) => Ok(s),
    Err(e) => return Err(e),
  }
}

fn read_username_from_file2() -> Result<String, io::Error> {
  let mut f = File::open("hello.txt")?;

  let mut s = String::new();
  f.read_to_string(&mut s)?;
  Ok(s)
}


fn read_username_from_file3() -> Result<String, io::Error> {
  let mut s = String::new();

  File::open("hello.txt")?.read_to_string(&mut s)?;

  Ok(s)
}



