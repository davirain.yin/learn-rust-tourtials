fn main() {
  //1 定义结构体
  //2 创建结构体示例
  //3 修改结构体字段
  //4 参数名字和字段名字同名的简写方法
  //5 从其他结构体创建示例
  //6 元组结构体
  // 字段没有名字
  // 使用圆括号
  //7 没有任何字段的单元结构体
  //8 打印结构体
  println!("**********************");
  
  let mut  user = User::new();
  println!("user = {:#?}", user);
  
  println!("**********************");
  
  user.mut_name("xiaoming".into());
  println!("user = {:#?}", user);
  
  println!("**********************");
  
  user.mut_count("234".into());
  println!("user = {:#?}", user);
  
  println!("**********************");
  
  user.mut_nonce(34);
  println!("user = {:#?}", user);
  
  println!("**********************");
  
  user.mut_active(true);
  println!("user = {:#?}", user);
  
  println!("**********************");
  
  println!("{}, {}, {}, {}", user.name(), user.count(), user.nonce(), user.active());
  
  println!("**********************");

  let point  = Point::new(2, 3);
  println!("point = {:?}", point);
  println!("**********************");
  let a = A;
  println!("a = {:?}", a);
  println!("**********************");


  let dog = Dog::new("wangcai".into(), 100.0, 75.0);

  println!("dog = {:?}", dog);
  println!("name = {}", dog.get_name());
  println!("weight = {}", dog.get_weight());
}

#[derive(Debug, Clone)]
struct User {
  name: String,
  count: String,
  nonce: u64,
  active: bool,
}

impl User {
  pub fn new() -> Self {
    Self {
      name: String::new(),
      count: String::new(),
      nonce: 0,
      active: false,
    }
  }

  pub fn mut_name(&mut self, name: String) {
    self.name = name;
  }
  pub fn mut_count(&mut self, count: String) {
    self.count = count;
  }
  pub fn mut_nonce(&mut self, nonce: u64) {
    self.nonce = nonce;
  }
  pub fn mut_active(&mut self, active: bool) {
    self.active = active;
  }
  pub fn name(&self) -> &str {
    &self.name
  }
  pub fn count(&self) -> &str {
    &self.count
  }
  pub fn nonce(&self) -> u64 {
    self.nonce
  }
  pub fn active(&self) -> bool {
    self.active
  }
}

#[derive(Debug)]
struct Point(i32, i32);


impl Point {
  pub fn new(a: i32, b: i32) -> Self {
    Point(a,b)
  }
}

#[derive(Debug)]
struct A;



#[derive(Debug)]
struct Dog{
  name: String,
  weight: f32, 
  height: f32,
}

impl Dog {
  fn new(name: String, weight: f32, height: f32) -> Self {
    Self{
      name,
      weight,
      height,
    }
  }
  fn get_name(&self) -> &str {
    &self.name
  }
  fn get_weight(&self) -> f32 {
    self.weight
  }

  fn get_height(&self) -> f32 {
    self.height
  }
}
