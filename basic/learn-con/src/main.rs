fn main() {
    println!("*************************");
    let b = true;
    if b {
      println!("b = {}", b);
    }
    println!("*************************");

    let y = 3;

    if y == 1 {
      println!("y = 1");
    }

    println!("*************************");
    
    if y == 1 {
      println!("y = 1");
    }else {
      println!("y != 1");
    }
    
    println!("*************************");

    if y == 1 {
      println!("y = 1");
    }else if y == 0 {
      println!("y == 0");
    }else if y == 2 {
      println!("y == 2");
    }else {
      println!("other!");
    }
    
    println!("*************************");
    
    let condition = true;
    let x = if condition { 5} else { 6};
    println!("x = {}", x);
    
    println!("*************************");

    let mut counter = 0;

    loop {
      println!("in loop");
      if counter == 10 {
        break;
      }

      counter += 1;
    }

    println!("counter = {}", counter);
    
    println!("*************************");


    let result = loop {
      counter +=1 ;
      if counter == 20 {
        break counter * 2;
      }
    };

    println!("result = {}", result);
    
    println!("*************************");

    let mut i = 0;

    while i != 10 {
      i += 1;
    }

    println!("i = {}", i);
    
    println!("*************************");

    let arr: [i32; 5] = [1, 2, 3, 4, 5];
    for elem in arr.iter() {
      println!("elem = {}", elem);
    }

    println!("*************************");
    
    for elem in &arr {
      println!("elem = {}", elem);
    }

    println!("*************************");
}
