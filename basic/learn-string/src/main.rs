#![allow(unused_variables)]
// 1 创建一个空的string
// 2 通过字面值创建一个string
// 2.1 使用string::from()
// 2.2 使用str的方式
// 3. 更新string
// 3.1 push_str
// 3.2 push
// 3.3. 使用 + 
// 3.4 使用format
// 4 String索引
// 5 str遍历
// 6 遍历
//
// 6.1 chars
// 6.2 btes
fn main() {
    println!("******************");
    let mut s = String::new();
    s.push_str("hello world");
    println!("s = {}", s);

    println!("******************");
    let s = String::from("hello world");
    println!("s = {}", s);

    println!("******************");
    let s  = "hello world".to_string();
    println!("s = {}", s);

    println!("******************");
    let mut s = String::from("hello");
    s.push_str(", world");

    let ss = " !".to_string();
    s.push_str(&ss);
    println!("s = {}", s);
    println!("ss = {}", ss);


    println!("******************");
    let mut s = String::from("tea");
    s.push('m');
    println!("s = {}", s);


    println!("******************");
    let s1 = "hello".to_string();
    let s2 = String::from(", world");
    let s3 = s1 + &s2;
    println!("s3 = {}", s3);
    //println!("s1 = {}", s1);
    println!("s2 = {}", s2);
    

    println!("******************");
    let s1 = String::from("tic");
    let s2 = String::from("tia");
    let s3 = String::from("tib");

    let s4 = format!("{}-{}-{}", s1, s2, s3);
    println!("s4 = {}", s4);
    

    println!("******************");
    let s = String::from("hello world");
    //let s2 = &s[1];
    println!("s len = {}", s.len());

    println!("******************");
    let s = String::from("你好");
    println!("s len = {}", s.len());

    println!("******************");
    let hello = "你好";
    let h5 = &hello[0..3];
    println!("hello = {}, h5 = {}", hello, h5);
    
    //let hello = "你好";
    //let h5 = &hello[0..2];
    //println!("hello = {}, h5 = {}", hello, h5);

    // chars
    //
    println!("******************");
    for c in s.chars() {
      println!("c = {}", c);
    }
    println!("******************");
    for b in s.bytes(){
      println!("b = {}", b);
    }
    println!("******************");
}
