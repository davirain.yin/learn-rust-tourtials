mod mod_a {
  
  pub struct A {
    pub number: i32,
    name: String,
  }
  
  impl A {

    pub fn new() -> Self {
      Self {
        number:0,
        name: String::from("A"),
      }
    }

    pub fn print_a(&self) {
      println!("numner = {}, name = {}", self.number, self.name);
    }
  }
  
  pub mod mod_b {
      pub fn print_b() {
        println!("B");
      }

      pub mod mod_c {
        pub fn print_c() {
          println!("C");
          super::print_b();
        }
      }
  }
}
fn main() {

    let a = mod_a::A::new();
    a.print_a();

    //let number = a.number;
    //let name = a.name;

    mod_a::mod_b::mod_c::print_c();
}
