// 1 HashMap<K,V>
// 2 创建HashMap
// 3 读取
// 4 遍历
// 5 更新
//
use std::collections::HashMap;
fn main() {
  println!("********************************************************");

  let mut scores : HashMap<String, i32> = HashMap::new();
  scores.insert(String::from("Blue"), 23);
  scores.insert(String::from("Red"), 20);
  println!("scores = {:?}", scores);
  
  println!("********************************************************");

  let keys = vec!["blue".to_string(), "Red".to_string()];
  let values = vec![10, 20];

  let scores: HashMap<_, _> = keys.iter().zip(values.iter()).collect();

  println!("scores = {:?}", scores);

  println!("********************************************************");

  let k = String::from("blue");
  let v = scores.get(&k);

  println!("v = {:?}", v);
  println!("v = {:?}", scores.get(&k).unwrap_or(&&23));
  
  println!("********************************************************");

  for (key, value) in scores.iter() {
    println!("{}, {}", key, value);
  }
  
  println!("********************************************************");
  
  let mut s = HashMap::new();
  s.insert(String::from("one"), 1);
  s.insert(String::from("two"), 2);
  s.insert(String::from("three"), 3);
  s.insert(String::from("one"), 4);
  println!("s = {:?}", s);
  
  println!("********************************************************");
  
  let mut s = HashMap::new();
  s.insert(String::from("one"), 1);
  s.insert(String::from("two"), 2);
  s.insert(String::from("three"), 3);
  s.entry(String::from("one")).or_insert(3);
  println!("s = {:?}", s);
  
  println!("********************************************************");

  // 根据旧值来更新一个值
  let text = "hello world wonderful world";

  let mut map = HashMap::new();
  for word in text.split_whitespace() {
    let count = map.entry(word).or_insert(0);
    *count += 1;
  }

  println!("map = {:?}", map);
  println!("********************************************************");
}
