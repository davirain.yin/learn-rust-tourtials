fn main() {
  // bool 
  let is_true: bool = true;
  println!("is true = {}", is_true);

  let is_false: bool = false;
  println!("is false = {}", is_false);
  
  // char, 32bit 
  let a = 'a';
  println!("a = {}", a);
  
  let b = '你';
  println!("b = {}", b);

  //i8, i16, i32, i64, u8, u16, u32, u64
  //f32, f64
  let c: i8 = -100;
  println!("c = {}", c);

  let d: f32 = 34.0;
  println!("d = {}", d);

  // 自适应类型 isize, usize
  println!("usize  max len = {}", usize::max_value());

  // 数组
  // [type; size]
  // size 也是数组类型的一部分
  let arr : [u32; 5] = [1, 2, 3, 4, 5];
  println!("arr = {:?}", arr);
  show(&arr);

  // 元组
  let tup: (i32, f32, char) = (23, 34.45, 'a');
  println!("tup = {:?}", tup);
  display_tuple1(tup);
  display_tuple2(tup);
}

fn show(arr: &[u32]){
  println!("arr = {:?}", arr);
  println!("-----------------");
  for i in arr {
    println!("{}", i);
  }
  println!("-----------------");
}

fn display_tuple1(tup: (i32, f32, char)) {
  println!("-----------------");
  println!("tup.0 = {}", tup.0);
  println!("tup.1 = {}", tup.1);
  println!("tup.2 = {}", tup.2);
  println!("-----------------");
}
fn display_tuple2((tup1, tup2, tup3): (i32, f32, char)) {
  println!("-----------------");
  println!("tup.0 = {}", tup1);
  println!("tup.1 = {}", tup2);
  println!("tup.2 = {}", tup3);
  println!("-----------------");
}
