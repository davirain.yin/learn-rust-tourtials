#![allow(dead_code)]
mod anomail {
    pub mod dog {
        pub fn hello() {
            println!("oooooo!!!!");
        }
        pub fn is_dog() -> bool {
            true
        }
    }
    pub mod cat {
        pub fn hello() {
            println!("cat cat!!!!");
        }
        pub fn is_cat() -> bool {
            true
        }
    }
}

#[cfg(test)]
mod tests {

    use super::anomail::cat;
    use super::anomail;
    #[test]
    fn uew_cat() {
        assert_eq!(true, cat::is_cat());
    }

    #[test]
    fn use_dog() {
      assert_eq!(true, anomail::dog::is_dog());
    }
}
