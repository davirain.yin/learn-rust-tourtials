#![allow(unused_variables)]
#![allow(dead_code)]
#![allow(unreachable_patterns)]
  // 1 类似C语言的方式定义
  #[derive(Debug)]
  enum IpAddrKind {
    V4,
    V6,
  }
  
  #[derive(Debug)]
  struct IpAddr {
    kind: IpAddrKind,
    address: String,
  
  }

  // 2 rust提倡的方式书写
  enum IpAddr2 {
    V4(String),
    V6(String),
  }
  
  // 3 可是是不同的类型
  enum IpAdd3 {
    V4(u8, u8, u8, u8),
    V6(String),
  }

  enum Message{
      Quit,
      Move{x: i32, y: i32},
      Write(String),
      Change(i32, i32, i32),
  }

  // 等同于
  //struct QuitMessage; // 类单元结构体
  //struct MoveMessage {
  //  x: i32,
  //  y: i32,
  //}
  //struct WriteMessage;
  //struct Change(i32, i32, i32)
  //

  // 5 枚举类型的方法及其match
  impl Message {
    fn print(&self) {
        match *self {
          Message::Quit => println!("Quit"),
          Message::Move{x, y} => println!("Move x = {}, y = {}", x, y),
          Message::Change(a, b, c) => println!("Change a = {}, b = {}, c = {}", a, b, c),
          Message::Write(ref s) => println!("Write = {}", s),
          _ => println!("default"),
        }
    }
  }

fn main() {

  let i1 = IpAddr {
    kind: IpAddrKind::V4,
    address: String::from("127.0.0.1"),
  };

  let i2 = IpAddr {
    kind: IpAddrKind::V6,
    address: String::from("::1"),
  };
  
  
  let i1 = IpAddr2::V4(String::from("127.0.0.1"));
  let i2 = IpAddr2::V6(String::from("::1"));

  
  let i1 = IpAdd3::V4(127, 0, 0, 1);
  let i2 = IpAdd3::V6(String::from("::1"));

  let message = Message::Quit;
  message.print();

  let me = Message::Move{x: 10, y: 20};
  me.print();

  let change = Message::Change(1, 2, 3);
  change.print();

  let w = Message::Write(String::from("hello world"));
  w.print();

}
