#![allow(unused_variables)]
#![allow(unused_mut)]
// 1 创建空的vector
// 2 创建 包含初始值的vector
// 3 丢弃 vector
// 4 读取元素
// 5 更新元素
// 6 遍历元素
// 7 使用枚举
fn main() {
    // 1
    // let v: Vec<i32> = Vec::new();
    // let mut v: Vec<i32> = Vec::new();
    // v.push(1);

    // 2
    let v = vec![1, 2, 3];

    // 3
    {
        let v = vec![1, 2, 3, 4, 5];
    }

    // 4

    println!("******************");
    let one: &i32 = &v[0];

    println!("one = {}", one);
    println!("one = {}", *one);

    println!("******************");
    match v.get(1) {
        Some(value) => println!("value = {}", value),
        _ => println!("None"),
    }

    // 5
    let mut v2: Vec<i32> = Vec::new();

    v2.push(1);
    v2.push(2);
    v2.push(3);

    println!("******************");
    // 6
    // 不可变的遍历
    // 可变的遍历
    //
    for i in v2.iter() {
        println!("i = {}", i);
    }

    for i in v2.iter_mut() {
        *i += 1;
    }

    println!("******************");
    for i in v2.iter() {
        println!("i = {}", i);
    }
    println!("******************");


    // 7
    //
    #[derive(Debug)]
    enum Context {
      Text(String),
      Float(f32),
      Int(i32),
    }

    let c = vec![
      Context::Text(String::from("string")),
      Context::Float(-1.0),
      Context::Int(23),
    ];
    println!("c = {:?}", c);


    //8 
    //
    let mut v = vec![1, 2, 3, 4];
    let first = &v[0];
    //v.push(6);

    println!("first = {}", first);
    println!("******************");
}
