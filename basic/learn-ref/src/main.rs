fn main() {
    println!("*******************");
    
    let s1 = gives_ownership();
    println!("s1 = {}", s1);
    
    println!("*******************");

    let s2 = String::from("hello");
    println!("s2 = {}", s2);
    
    println!("*******************");

    let s3 = takes_and_gives_back(s2);
    println!("s3 = {}", s3);
    
    println!("*******************");
    //println!("s2 = {}", s2); // s2 have ben moved 

    let s1 = String::from("hello");
    let len = calcute_length(&s1);
    
    println!("s1 length = {}", len);
    println!("s1 = {}", s1); 
    
    println!("*******************");
    
    let mut s2 = String::from("hello");
    println!("modify before, s2 = {}", s2);
    
    modify_str(&mut s2);
    
    println!("modify after, s2 = {}", s2);
    
    println!("*******************");
}


fn gives_ownership() -> String {
  let s = String::from("hello");
  s
}

fn takes_and_gives_back(s: String) -> String {
  s
}

// 引用 & 
// 引用用法：用&， 让我们创建一个指向值得引用，但是并不拥有它，因为不拥有这个值，
// 所有当引用离开其指向的作用域后也不会被丢弃
// 当对于一个对象的借用发生时，借用之前的引用全部失效
// 1. 在任意给定时间，在有了可变引用之后不能有不可变引用
// 2.悬垂引用
fn calcute_length(s: &str) -> usize {
  s.len()
}

fn modify_str(s: &mut String) {
  s.push_str(", world");
}

/// fn dangle() -> &String;
/// ```rust 
/// let ref_s = dangle();
/// println!("ref_s =  {}", ref_s);
/// ```
fn dangle(s: &str) -> &str {
  let s1 = String::from("hello");
  println!("s1 = {}", s1);
  s
}

