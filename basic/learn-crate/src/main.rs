#![allow(dead_code)]
mod factory {
  pub mod product_refrigerator {
    pub fn produce_re() {
      println!("produce refigerator!");
    }
  }

  pub mod produce_washing_machine {
    pub fn produce_washing_machine() {
      println!("produce washing machine!");
    }
  }
}


fn main() {
  factory::product_refrigerator::produce_re();

  factory::produce_washing_machine::produce_washing_machine();


   println!("Hello, world!");
}
