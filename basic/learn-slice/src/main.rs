#![allow(unused_variables)]
// 1. 字符串是String中的一部分的值得引用
// 2. 字面值就是slice
// 3. 其他类型的slice
fn main() {
  println!("**********************");
  let s = String::from("hello,world");

  let h = &s[0..5];
  let h = &s[0..=4];
  let h = &s[..5];
  let h = &s[..=4];


  let w = &s[6..11];
  let s = &s[..];
   
  println!("s = {}", s);
  println!("w = {}", w);
  println!("h = {}", h);

  println!("**********************");
  
  let ss = String::from("你好");

  //let w1 = &ss[0..1];
  //

  let s3 = "hello, world";


  let a = [1, 2, 3, 4, 5];

  let a1 = &a[1..3];
  println!("a1 = {:?}", a1);
  println!("**********************");
}
