// Option 是标准库定义的一个枚举，
// enum Option<T> {
//  Some(T),
//  None,
//  }
//
//
// 2 使用方式
#![allow(unused_variables)]
fn main() {
    let some_number = Some(5);
    let some_string = Some(String::from("hello world"));
    let absent_number: Option<i32> = None;

    let x: i32 = 5;
    let y: Option<i32> = Some(5);

    let mut temp = 0;

    match y {
        Some(i) => {
            temp = i;
        }
        None => {
            println!("do nothing");
        }
    }

    let sum = x + temp;
    println!("sum = {}", sum);

    //let result = plus_one(y);
    //match result {
    //  Some(ret) => println!("result = {}", ret),
    //  None => println!("none"),
    //}
    //
    //if let Some(value) = plus_one(y) {
    //    println!("result = {}", value);
    //}
    
    plus_one(y).map_or_else(|| println!("none"), |val| println!("result = {}", val));

    println!("Hello, world!");
}

fn plus_one(x: Option<i32>) -> Option<i32> {
    //match x {
    //  None => None,
    //  Some(x) => Some(x + 1),
    //}
    x.map(|val| val + 1)
}
