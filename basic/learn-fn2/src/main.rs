
fn main() {
    println!("*********************");
    let s = String::from("hello, world");

    let s =  takes_ownership(s);
    println!("s  = {}", s);

    println!("*********************");

    let x = 5;

    makes_copy(x);
    println!("x = {}", x);
    
    println!("*********************");
}


fn makes_copy(i: i32) {
  println!("i = {}", i);
}

fn takes_ownership(some_string: String) -> String {
  println!("{}", some_string);
  some_string
}
